# nafpay-fix

My Money (also known as NAFPay) attempts to send you to a domain accessible only on the Air Force network. This extension redirects those requests to the public nafpay.afsv.net.

## Note: No longer being maintained

I am no longer employed by the Air Force and do not maintain this extension. Feel free to fork it.

## Installation:

### Firefox:

Download the XPI file from the [Releases](https://gitlab.com/soupcan/nafpay-fix/releases) section.

### Chrome/ium:

No longer available from the Chrome Web Store.

## Additional Resources

To prevent HTTPS certificate errors, download and install the InstallRoot tool from [DISA](https://iase.disa.mil/pki-pke/Pages/tools.aspx) under the "Trust Store" tab.
