// Modified from examples on the Mozilla Developer Network

function redirect(requestDetails) {

  correctedUrl = requestDetails.url.replace("http://","https://")
  correctedUrl = correctedUrl.replace("https://afsfms.afsv.net","https://nafpay.afsv.net");

  return {
    redirectUrl: correctedUrl
  };

}

chrome.webRequest.onBeforeRequest.addListener(
  redirect,
  {urls: ["*://afsfms.afsv.net/*", "http://nafpay.afsv.net/*"]},
  ["blocking"]
);
